<?php
/**
 * Created by PhpStorm.
 * User: calvincheung
 * Date: 24/10/2017
 * Time: 3:47 PM
 */
session_start();
$link = htmlentities($_POST['link']);
$comment = htmlentities($_POST['comment']);
$css = htmlentities($_POST['css']);
$file = "liens.html";

if (filter_var($link, FILTER_VALIDATE_URL)) {
    if (!file_exists($file)) {
        touch($file);
    }
    if ($f = fopen($file, "a+")) {
        $test = @fopen($link, 'r');
        // Split up string after every slash and join them with anti-slash and slash to make a regular expression test
        $test_string = explode("/", $link);
        $regex_test = '/' . implode("\\/", $test_string) . '/';
        $result = false;
        // Return $result = true if $line matches the regular expression
        while (!feof($f)) {
            $line = fgets($f);
            if (preg_match($regex_test, $line)) {
                $result = true;
            }
        }
        if ($result === false) {
            if ($test) {
                $code = "<li><a href='$link'>$comment - $link</a>\n";
                fputs($f, $code);
                fclose($f);
                $_SESSION['message'] = "Votre lien est ajouté !";
                $_SESSION['status'] = "success";
                header("Location: index.php?page=links&css=$css&title=Liens");
                exit();
            } else {
                fclose($f);
                $_SESSION['message'] = "Votre lien n'est pas valide !";
                $_SESSION['status'] = "warning";
                header("Location: index.php?page=links&css=$css&title=Liens");
                exit();
            }
        } else {
            fclose($f);
            $_SESSION['message'] = "Votre lien existe déjà !";
            $_SESSION['status'] = "warning";
            header("Location: index.php?page=links&css=$css&title=Liens");
            exit();
        }
    }
} else {
    $_SESSION['message'] = "Votre lien n'est pas valide !";
    $_SESSION['status'] = "warning";
    header("Location: index.php?page=links&css=$css&title=Liens");
    exit();
}

