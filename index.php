<?php
/**
 * Created by PhpStorm.
 * User: calvincheung
 * Date: 24/10/2017
 * Time: 1:40 PM
 */
$page = htmlentities($_GET['page']);
$css = htmlentities($_GET['css']);
$title = htmlentities($_GET['title']);

if (!isset($_GET['page'])||!isset($_GET['css'])||!isset($_GET['title'])) {
    $page = "home";
    $title = "Home";
    $css = "css0";
}

?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title ?> | The Time Travellers</title>
    <!--  BXSlider  -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
    <!--  Google Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=VT323" rel="stylesheet">
    <!--  Custom CSS  -->
    <link type="text/css" rel="stylesheet" href="static/<?= $css ?>.css">

</head>
<body class="future-global-grid">
    <video id="future-video" autoplay loop poster="static/img/space.png">
        <source src="static/video/Star-Field-and-Warp.mov" type="video/mp4">
    </video>

<?php
    include_once "pages/partials/header.php";
    echo "\n";
    include_once "pages/partials/nav.php";
    echo "\n";

    // Avoid XSS security loophole
    switch ($page) {
        case "home":
            include_once "pages/home.php";
            break;
        case "about":
            include_once "pages/about.php";
            break;
        case "animation":
            include_once "pages/animation.php";
            break;
        case "flexbox":
            include_once "pages/flexbox.php";
            break;
        case "responsive-design":
            include_once "pages/responsive-design.php";
            break;
        case "links":
            include_once "pages/links.php";
            break;
        default:
            include_once "pages/home.php";
    }

    echo "\n";
    include_once "pages/partials/footer.php";
?>

    <!-- BXSlider  -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
    <script src="static/js/main.js"></script>
</body>
</html>
