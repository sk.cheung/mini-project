<?php



?>

<div class="container bg-muted">
    <?php if ($css === 'css0'): ?>
        <h1>À Propos</h1>
    <?php elseif ($css === 'css1'): ?>
    <section class="hero-section about-hero">
        <section class="hero-overlay">
            <h1 class="animated fadeInLeft">À Propos</h1>
        </section>
    </section>
    <?php elseif ($css === 'css2'): ?>
        <section class="banner animated slideInUp">
            <img class="img-responsive" src="static/img/ttt-banniere.png" alt="Time Travellers Banner">
        </section>
        <h1 class="animated fadeInLeft">À Propos</h1>
    <?php endif; ?>
    <p>Nous sommes un groupe de voyageurs dans le temps expérimentés. Nous avons pris le temps de vous partager notre savoir sur le passé et le futur, acquis lors de nos nombreuses voyages dans de différentes époques, avec vous, qui êtes dans le présent.</p>
</div>

