<?php
/**
 * Created by PhpStorm.
 * User: calvincheung
 * Date: 24/10/2017
 * Time: 2:10 PM
 */

?>
<div class="container">
    <?php if ($css === 'css1'): ?>
        <section class="hero-section flexbox-hero">
            <section class="hero-overlay">
                <h1 class="animated fadeInLeft">Flexbox</h1>
            </section>
        </section>
    <?php else: ?>
        <h1 class="animated fadeInLeft">Flexbox</h1>
    <?php endif; ?>
</div>
