<?php
/**
 * Created by PhpStorm.
 * User: calvincheung
 * Date: 24/10/2017
 * Time: 1:48 PM
 */

?>


<section class="wrapper">
    <div class="bxslider">
        <div><img src="static/img/astronaut.jpg" alt=""></div>
        <div><img src="static/img/einstein.jpg" alt=""></div>
    </div>

    <div class="container bg-muted">
        <p>Bonjour, et bienvenue dans notre univers! Tu es ici car tu penses que voyager dans le temps est possible et que tu cherches à comprendre comment effectuer un voyage dans le temps? HTML4 et CSS2 c’est du passé. Alors prends le temps de faire un petit tour dans le passé et dans le futur pour découvrir comment voyager dans le temps à l’aide des nouveautés de HTML5 et CSS3 venu droit du futur! Sur ce site le déroulement linéaire du temps est suspendu! Mais viens, dépêche toi, il est temps de découvrir les nouveautés du web!</p>
    </div>
</section>
