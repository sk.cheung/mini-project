<?php
/**
 * Created by PhpStorm.
 * User: calvincheung
 * Date: 24/10/2017
 * Time: 3:40 PM
 */

// Unset and delete section if last_activity is more than 30s
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] >= 30)) {
    session_unset();
    session_destroy();
    session_start();
} else {
    session_start();
    $_SESSION['LAST_ACTIVITY'] = time();
}

?>

    <div class="container bg-muted">
        <?php if ($css === 'css1'): ?>
            <section class="hero-section links-hero">
                <section class="hero-overlay">
                    <h1 class="animated fadeInLeft">Liens utiles</h1>
                </section>
            </section>
        <?php else: ?>
            <h1 class="animated fadeInLeft">Liens utiles</h1>
        <?php endif; ?>

        <ul class="links">
            <?php include_once "liens.html"; ?>
        </ul>
        <h2>Soumettez vos lien!</h2>
        <?php if ($_SESSION['message'] !== null) : ?>
            <div class="message <?= $_SESSION['status'] === 'warning' ? 'warning' : 'success' ?>">
                <p><?= $_SESSION['message'] ?></p>
            </div>
        <?php endif; ?>
        <form id="link-form" action="../add_links.php" method="POST">
            <label for="link">Lien</label>
            <input type="text" name="link" id="link" value="http://">
            <label for="comment">Commentaire</label>
            <input type="text" name="comment" id="comment">
            <input type="hidden" name="css" value="<?= $css ?>">
            <button type="submit">Soumettre</button>
        </form>
    </div>
