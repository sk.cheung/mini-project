<?php
/**
 * Created by PhpStorm.
 * User: calvincheung
 * Date: 24/10/2017
 * Time: 1:43 PM
 */

?>

<footer>
    <ul>
        <li><a href="?page=<?= $page ?>&css=css0&title=<?= $title ?>" class="<?= $_GET['css'] == 'css0' ? 'active' : ''; ?>">Past | CSS 1</a></li>
        <li><a href="?page=<?= $page ?>&css=css1&title=<?= $title ?>" class="<?= $_GET['css'] == 'css1' ? 'active' : ''; ?>">Modern | CSS 2</a></li>
        <li><a href="?page=<?= $page ?>&css=css2&title=<?= $title ?>" class="<?= $_GET['css'] == 'css2' ? 'active' : ''; ?>">Future | CSS 3</a></li>
    </ul>
    <p id="copyright">Copyright &copy; - Siu Kei & Laura - CAWEB 2017</p>
</footer>
