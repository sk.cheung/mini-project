<?php
/**
 * Created by PhpStorm.
 * User: calvincheung
 * Date: 24/10/2017
 * Time: 1:44 PM
 */



?>

<nav>
    <section class="nav-header">
        <?php if ($css === 'css0'): ?>
        <a href="?page=home&css=<?= $css ?>&title=Home" class="navbar-brand css-past-logo"><img src="static/img/thetimetravellers.png" alt="The Time Travelers Logo"></a>
        <?php elseif ($css === 'css1'): ?>
        <a href="?page=home&css=<?= $css ?>&title=Home" class="navbar-brand css-present-logo"><img src="static/img/thetimetravellersur1ligne.png" alt="The Time Travelers Logo"></a>
        <?php else: ?>
        <a href="?page=home&css=<?= $css ?>&title=Home" class="navbar-brand css-future-logo"><img src="static/img/logo-v2-fond-blanc.svg" alt="The Time Travelers Logo"></a>
        <?php endif; ?>
        <a href="#" id="nav-mobile">&#9776; MENU</a>
    </section>
    <ul>
        <li><a href="?page=home&css=<?= $css ?>&title=Home" class="<?= $_GET['page'] == "home" ? "active" : "" ?>">Home</a></li>
        <li><a href="?page=about&css=<?= $css ?>&title=About" class="<?= $_GET['page'] == "about" ? "active" : "" ?>">About</a></li>
        <li><a href="?page=responsive-design&css=<?= $css ?>&title=Responsive&nbsp;Design" class="<?= $_GET['page'] === "responsive-design" ? "active" : "" ?>">Responsive Design</a></li>
        <li><a href="?page=flexbox&css=<?= $css ?>&title=Flexbox" class="<?= $_GET['page'] == "flexbox" ? "active" : "" ?>">Flexbox</a></li>
        <li><a href="?page=animation&css=<?= $css ?>&title=Animation" class="<?= $_GET['page'] == "animation" ? "active" : "" ?>">Animation</a></li>
        <li><a href="?page=links&css=<?= $css ?>&title=Liens" class="<?= $_GET['page'] == "links" ? "active" : "" ?>">Liens</a></li>
    </ul>
</nav>



