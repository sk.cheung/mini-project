<?php
/**
 * Created by PhpStorm.
 * User: calvincheung
 * Date: 24/10/2017
 * Time: 2:10 PM
 */

?>
<div class="container bg-muted">
    <?php if ($css === 'css1'): ?>
        <section class="hero-section responsive-hero">
            <section class="hero-overlay">
                <h1 class="animated fadeInLeft">Responsive Web Design</h1>
            </section>
        </section>
    <?php else: ?>
        <h1 class="animated fadeInLeft">Responsive Web Design</h1>
    <?php endif; ?>
    <div class="row">
        <p>Différentes époques, différents appareils… Toi qui viens du passé, tu ne le sais pas encore. Mais dans un future proche, qui est le présent à présent  il n’y aura pas seulement des écrans d’ordinateur. Mais de nouveaux appareils seront inventés!</p>
    </div>

    <div class="row">
        <h2>Alors qu'est-ce que le responsive design?</h2>
        <p>Le responsive design est une technique actuelle qui permet, à l’aide de HTML5 et CSS3 Media Queries, l’affichage uniforme du contenu d’un site web. Le layout est donc conçu de manière flexible pour qu’il s’affiche de la même manière sur ordinateur, tablette et smartphone et pour assurer une simplicité d’usage.</p>
    </div>

    <div class="row">
        <h2>“Form follows function”</h2>
        <p>Dans le responsive webdesign les fonctionnalités, le design et le contenu suivent la résolution de l’écran utilisé. Responsive webdesign, du design réactif. Les éléments de contenu et de navigation tout comme la structure s’adaptent pour correspondre au dispositif.</p>
    </div>

    <div class="row">
        <h2>Résolution standard de smartphones et tablettes:</h2>
        <ul>
            <li>Smartphones: 320px à 480px</li>
            <li>Tablettes: 768px à 1024px</li>
            <li>Ordinateur: 1024px+</li>
        </ul>
    </div>
</div>
