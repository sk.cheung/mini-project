$(function(){
    // Slider
   $('.bxslider').bxSlider({
       mode: 'fade',
       caption: false,
       responsive: true,
       slideWidth: 600,
       pager: false
   });

   // Animations
   $('#link-form').addClass('animated fadeInUp');
});

// Mobile Navigation Bar
var navMenu = false;
$('#nav-mobile').on('click', function() {
    if (!navMenu) {
        $('nav ul').slideDown();
        $('nav ul').addClass('nav-responsive');
        $('nav ul li a').addClass('nav-responsive');
        navMenu = true;
    } else {
        $('nav ul').slideUp();
        $('nav ul').removeClass('nav-responsive');
        $('nav ul li a').removeClass('nav-responsive');
        navMenu = false;
    }
});